﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using WebApiTest.Models;

namespace WebApiTest.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NameCodesController : ApiController
    {
        private NameCodeContext db = new NameCodeContext();

        // GET: api/NameCodes
        public IQueryable<NameCode> GetNameCodes()
        {
            return db.NameCodes;
        }

        // GET: api/NameCodes/5
        [ResponseType(typeof(NameCode))]
        public IHttpActionResult GetNameCode(int id)
        {
            NameCode nameCode = db.NameCodes.Find(id);
            if (nameCode == null)
            {
                return NotFound();
            }

            return Ok(nameCode);
        }

        // PUT: api/NameCodes/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult PutNameCode(int id, NameCode nameCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nameCode.Id)
            {
                return BadRequest();
            }

            db.Entry(nameCode).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NameCodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/NameCodes
        [ResponseType(typeof(NameCode))]
        public IHttpActionResult PostNameCode(NameCode nameCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.NameCodes.Add(nameCode);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = nameCode.Id }, nameCode);
        }

        // DELETE: api/NameCodes/5
        [ResponseType(typeof(NameCode))]
        public IHttpActionResult DeleteNameCode(int id)
        {
            NameCode nameCode = db.NameCodes.Find(id);
            if (nameCode == null)
            {
                return NotFound();
            }

            db.NameCodes.Remove(nameCode);
            db.SaveChanges();

            return Ok(nameCode);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NameCodeExists(int id)
        {
            return db.NameCodes.Count(e => e.Id == id) > 0;
        }
    }
}