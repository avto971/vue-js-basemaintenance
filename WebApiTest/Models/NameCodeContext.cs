﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApiTest.Models
{
    public class NameCodeContext:DbContext
    {
        public NameCodeContext():base("NameCodeConnectionString")
        {

        }

        public DbSet<NameCode> NameCodes { get; set; }
    }
}