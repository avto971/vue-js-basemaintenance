namespace WebApiTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inital2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.NameCodes", "Name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.NameCodes", "Name", c => c.Int(nullable: false));
        }
    }
}
